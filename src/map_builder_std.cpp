// =============================================================================
// Copyright Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include <algorithm>
#include <cstddef>
#include <iterator>
#include <memory>
#include <ostream>
#include <vector>

#include "actor.hpp"
#include "array2.hpp"
#include "colors.hpp"
#include "debug.hpp"
#include "game_time.hpp"
#include "global.hpp"
#include "map.hpp"
#include "map_builder.hpp"
#include "map_controller.hpp"
#include "map_parsing.hpp"
#include "mapgen.hpp"
#include "panel.hpp"
#include "populate_items.hpp"
#include "populate_monsters.hpp"
#include "populate_traps.hpp"
#include "pos.hpp"
#include "random.hpp"
#include "room.hpp"
#include "state.hpp"
#include "terrain.hpp"
#include "terrain_data.hpp"
#include "terrain_door.hpp"
#include "terrain_event.hpp"
#include "terrain_factory.hpp"

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
static Array2<Region> init_regions()
{
        TRACE << "Init regions" << std::endl;

        const int split_x_interval = map::w() / 3;
        const int split_y_interval = map::h() / 3;

        const int split_x1 = split_x_interval;
        const int split_x2 = (split_x_interval * 2) + 1;

        const int split_y1 = split_y_interval;
        const int split_y2 = split_y_interval * 2;

        std::vector<int> x0_list = {1, split_x1 + 1, split_x2 + 1};
        std::vector<int> x1_list = {split_x1 - 1, split_x2 - 1, map::w() - 2};
        std::vector<int> y0_list = {1, split_y1 + 1, split_y2 + 1};
        std::vector<int> y1_list = {split_y1 - 1, split_y2 - 1, map::h() - 2};

        Array2<Region> regions(3, 3);

        for (int x_region = 0; x_region < 3; ++x_region) {
                const int x0 = x0_list[x_region];
                const int x1 = x1_list[x_region];

                for (int y_region = 0; y_region < 3; ++y_region) {
                        const int y0 = y0_list[y_region];
                        const int y1 = y1_list[y_region];

                        regions.at(x_region, y_region) = Region({x0, y0, x1, y1});
                }
        }

        return regions;
}

// -----------------------------------------------------------------------------
// MapBuilderStd
// -----------------------------------------------------------------------------
bool MapBuilderStd::build_specific()
{
        TRACE_FUNC_BEGIN;

        const int river_one_in_n = 12;

        const bool is_river_level =
                (map::g_dlvl >= g_dlvl_first_mid_game) &&
                rnd::one_in(river_one_in_n);

        // TODO: Using hard coded values for now. The values could probably be
        // randomized within a range.

        if (is_river_level) {
                // "River" levels need to be big so there won't be a tiny chasm.
                map::reset({60, 60});
        }
        else if (map::g_dlvl >= g_dlvl_first_late_game) {
                // Make late game levels slightly bigger.
                map::reset({54, 54});
        }
        else {
                map::reset({48, 48});
        }

        mapgen::g_is_map_valid = true;

        mapgen::g_door_proposals.resize(map::dims());

        // NOTE: This must be called before any rooms are created
        room::init_room_bucket();

        Array2<Region> regions = init_regions();

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Reserve regions for a "river"
        // ---------------------------------------------------------------------
        if (is_river_level) {
                mapgen::reserve_river(regions);
        }

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Merge some regions
        // ---------------------------------------------------------------------
        mapgen::merge_regions(regions);

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Make main rooms
        // ---------------------------------------------------------------------
        TRACE << "Making main rooms" << std::endl;

        for (int x = 0; x < 3; ++x) {
                for (int y = 0; y < 3; ++y) {
                        Region& region = regions.at(x, y);

                        if (!region.main_room && region.is_free) {
                                mapgen::make_room(region);
                        }
                }
        }

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // If there are too few rooms at this point, invalidate the map
        // ---------------------------------------------------------------------
        const size_t min_nr_rooms = 5;

        if (map::g_room_list.size() < min_nr_rooms) {
                mapgen::g_is_map_valid = false;

                return false;
        }

        // ---------------------------------------------------------------------
        // Make auxiliary rooms
        // ---------------------------------------------------------------------
        mapgen::make_aux_rooms(regions);

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Set all floor and walls to cave in late game
        // ---------------------------------------------------------------------
        if (map::g_dlvl >= g_dlvl_first_late_game) {
                for (const P& p : map::rect().positions()) {
                        terrain::Terrain* const t = map::g_terrain.at(p);

                        switch (t->id()) {
                        case terrain::Id::floor: {
                                static_cast<terrain::Floor*>(t)->m_type =
                                        terrain::FloorType::cave;
                        } break;

                        case terrain::Id::wall: {
                                static_cast<terrain::Wall*>(t)->m_type =
                                        terrain::WallType::cave;
                        } break;

                        default: {
                        } break;
                        }
                }
        }

        // ---------------------------------------------------------------------
        // BSP split rooms
        // ---------------------------------------------------------------------
        // NOTE: Occasionally bsp splitting rooms is allowed late game as well,
        // as it looks really good, and gives a vague sense of ruins - but it
        // leads to less open maps.
        if ((map::g_dlvl <= g_dlvl_last_mid_game) || rnd::one_in(3)) {
                mapgen::bsp_split_rooms();
        }

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Make sub-rooms
        // ---------------------------------------------------------------------
        mapgen::make_sub_rooms();

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Sort rooms according to room type
        // ---------------------------------------------------------------------
        // NOTE: This allows common rooms to assume that they are rectangular
        // and have their walls untouched when their reshaping functions run.

        std::sort(
                std::begin(map::g_room_list),
                std::end(map::g_room_list),
                [](const room::Room* const r0, const room::Room* const r1) {
                        return r0->m_type < r1->m_type;
                });

        // ---------------------------------------------------------------------
        // Run the pre-connect hook on all rooms
        // ---------------------------------------------------------------------
        TRACE << "Running pre-connect for all rooms" << std::endl;

        for (room::Room* const room : map::g_room_list) {
                room->on_pre_connect(mapgen::g_door_proposals);
        }

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Connect the rooms
        // ---------------------------------------------------------------------
        mapgen::connect_rooms();

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Run the post-connect hook on all rooms
        // ---------------------------------------------------------------------
        TRACE << "Running post-connect for all rooms" << std::endl;

        for (room::Room* const room : map::g_room_list) {
                room->on_post_connect(mapgen::g_door_proposals);
        }

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Place doors
        // ---------------------------------------------------------------------
        if (map::g_dlvl <= g_dlvl_last_mid_game) {
                mapgen::make_doors();
        }

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Run the affect surroundings hook on all rooms
        // ---------------------------------------------------------------------
        TRACE << "Running affect surroundings for all rooms" << std::endl;

        // This is executed in random room order, since nearby rooms may create
        // overlapping areas of terrain, and it shouldn't depend on creation
        // order which room goes first.
        std::vector<room::Room*> rooms_copy = map::g_room_list;

        rnd::shuffle(rooms_copy);

        for (room::Room* const room : rooms_copy) {
                room->affect_surroundings();
        }

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Set player position
        // ---------------------------------------------------------------------
        map::g_player->m_pos =
                P(rnd::range(1, map::w() - 2),
                  rnd::range(1, map::h() - 2));

        mapgen::move_player_to_nearest_allowed_pos();

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Decorate the map
        // ---------------------------------------------------------------------
        mapgen::decorate();

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Place the stairs
        // ---------------------------------------------------------------------
        // NOTE: The choke point information gathering below depends on the
        // stairs having been placed.
        mapgen::make_stairs_at_random_pos();

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Gather data on choke points in the map (check every position where a
        // door has previously been "proposed")
        // ---------------------------------------------------------------------
        mapgen::calc_chokepoint_data();

        TRACE
                << "Found "
                << "'" << map::g_chokepoint_data.size() << "' "
                << "choke points" << std::endl;

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Make warded doors and crystals
        // ---------------------------------------------------------------------
        mapgen::make_warded_doors_and_keys();

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Make some doors leading to "optional" areas secret or stuck
        // ---------------------------------------------------------------------
        for (const ChokePointData& chokepoint : map::g_chokepoint_data) {
                if (chokepoint.player_side != chokepoint.stairs_side) {
                        continue;
                }

                terrain::Terrain* const terrain = map::g_terrain.at(chokepoint.p);

                if (terrain->id() == terrain::Id::door) {
                        auto* const door = static_cast<terrain::Door*>(terrain);

                        if ((door->type() != terrain::DoorType::gate) &&
                            !door->is_warded() &&
                            rnd::one_in(6)) {
                                door->set_secret();
                        }

                        if ((door->type() != terrain::DoorType::metal) &&
                            !door->is_warded() &&
                            rnd::one_in(6)) {
                                door->set_stuck();
                        }
                }
        }

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Place Monoliths
        // ---------------------------------------------------------------------
        // NOTE: This depends on choke point data having been gathered
        // (including player side and stairs side)
        mapgen::make_monoliths();

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Place Mirrors
        // ---------------------------------------------------------------------
        // NOTE: This depends on choke point data having been gathered
        // (including player side and stairs side)
        if (map::g_dlvl <= g_dlvl_last_mid_game) {
                mapgen::make_mirrors();
        }

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Place pylons
        // ---------------------------------------------------------------------
        mapgen::make_pylons();

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Put inscribed terrain
        // ---------------------------------------------------------------------
        mapgen::put_inscribed_terrain();

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Populate the map with monsters
        // ---------------------------------------------------------------------
        for (const room::Room* const room : map::g_room_list) {
                room->populate_monsters();
        }

        populate_mon::populate_std_lvl();

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Populate the map with traps
        // ---------------------------------------------------------------------
        populate_traps::populate();

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Populate the map with items on the floor
        // ---------------------------------------------------------------------
        populate_items::make_items_on_floor();

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Place "snake emerge" events
        // ---------------------------------------------------------------------
        const int nr_snake_emerge_events_to_try =
                rnd::one_in(60)
                ? 2
                : rnd::one_in(16)
                ? 1
                : 0;

        for (int i = 0; i < nr_snake_emerge_events_to_try; ++i) {
                auto* const event =
                        static_cast<terrain::EventSnakeEmerge*>(
                                terrain::make(
                                        terrain::Id::event_snake_emerge,
                                        {-1, -1}));

                if (event->try_find_p()) {
                        game_time::add_mob(event);
                }
                else {
                        delete event;
                }
        }

        if (!mapgen::g_is_map_valid) {
                return false;
        }

        // ---------------------------------------------------------------------
        // Occasionally make the whole level dark
        // ---------------------------------------------------------------------
        if (map::g_dlvl > 1) {
                int make_drk_pct = 0;

                if (map::g_dlvl <= g_dlvl_last_early_game) {
                        make_drk_pct = 1;
                }
                else if (map::g_dlvl <= g_dlvl_last_mid_game) {
                        make_drk_pct = 2;
                }
                else {
                        make_drk_pct = 15;
                }

                if (rnd::percent(make_drk_pct)) {
                        const size_t nr_positions = map::nr_positions();
                        for (size_t i = 0; i < nr_positions; ++i) {
                                map::g_dark.at(i) = true;
                        }
                }
        }

        // ---------------------------------------------------------------------
        // Sanity check room sizes
        // ---------------------------------------------------------------------
        for (room::Room* const room : map::g_room_list) {
                if (room->m_type >= room::RoomType::END_OF_STD_ROOMS) {
                        continue;
                }

                if (room->m_r.min_dim() <= 1) {
                        ASSERT(false);

                        // Invalidate the map
                        mapgen::g_is_map_valid = false;
                }
        }

        if (!mapgen::g_is_map_valid) {
                return false;
        }

#ifdef NDEBUG
        for (room::Room* const room : map::g_room_list) {
                delete room;
        }

        map::g_room_list.clear();

        map::g_room_map.resize({0, 0});
#endif  // NDEBUG

        TRACE_FUNC_END;

        return mapgen::g_is_map_valid;
}

std::unique_ptr<MapController> MapBuilderStd::map_controller() const
{
        return std::make_unique<MapControllerStd>();
}
