To add fonts, place them in this directory.

For a font to be registered by the game, it must start with
<width>x<height>, and end with .png (e.g. "7x13_uushi.png").
