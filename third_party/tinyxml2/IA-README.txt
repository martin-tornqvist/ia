"tinyxml2.cpp" and "tinyxml2.h" are copied from:

https://github.com/leethomason/tinyxml2

Tag: "10.0.0"

Last commits:
------------------------------------------------------------------------
commit 321ea88 (HEAD, tag: 10.0.0)
Merge: c2d3087 26985b5
Author: Lee Thomason <leethomason@gmail.com>
Date:   Sat Dec 30 18:08:30 2023 -0800

    Merge pull request #965 from leethomason/v10.0.0
    
    V10.0.0

commit 26985b5 (origin/v10.0.0)
Author: Lee Thomason <leethomason@gmail.com>
Date:   Sat Dec 30 18:03:36 2023 -0800

    new docs

commit 35099af
Author: Lee Thomason <leethomason@gmail.com>
Date:   Sat Dec 30 17:56:34 2023 -0800

    updating version
------------------------------------------------------------------------

See LICENSE.txt and top of tinyxml source files for license information.
