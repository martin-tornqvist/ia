// =============================================================================
// Copyright Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef GFX_HPP
#define GFX_HPP

#include <string>

struct P;

namespace gfx
{
// NOTE: When updating this, also update the translation tables in the cpp file.
enum class TileId
{
        aim_marker_head,
        aim_marker_line,
        alchemist_bench_empty,
        alchemist_bench_full,
        altar,
        ammo,
        amoeba,
        amulet,
        ape,
        armor,
        astral_opium,
        axe,
        barrel,
        bat,
        beast,
        blast1,
        blast2,
        bog_tcher,
        bone_charm,
        bookshelf_empty,
        bookshelf_full,
        brazier,
        bush,
        byakhee,
        cabinet_closed,
        cabinet_open,
        chains,
        chest_closed,
        chest_open,
        chthonian,
        church_bench,
        clockwork,
        club,
        cocoon_closed,
        cocoon_open,
        corpse,
        corpse2,
        corpse_mound,
        crawling_hand,
        crawling_intestines,
        croc_head_mummy,
        crowbar,
        crystal,
        crystal_destroyed,
        cultist_dagger,
        cultist_pistol,
        cultist_pump_shotgun,
        cultist_rifle,
        cultist_sawed_off_shotgun,
        cultist_spiked_mace,
        cultist_tommygun,
        dagger,
        deep_one,
        device1,
        device2,
        door_closed,
        door_open,
        door_stuck,
        door_warded,
        drop,
        dynamite,
        dynamite_lit,
        elder_sign,
        electric_gun,
        excl_mark,
        failed_reanimation,
        fiend,
        flare,
        flare_gun,
        flare_lit,
        floating_skull,
        floor,
        fluctuating_material,
        fountain,
        fungi,
        gas_mask,
        gas_spore,
        gate_closed,
        gate_open,
        gate_stuck,
        ghost,
        ghoul,
        giant_spider,
        glasuu,
        goat,
        gong,
        gore1,
        gore2,
        gore3,
        gore4,
        gore5,
        gore6,
        gore7,
        gore8,
        grate,
        grave_stone,
        hammer,
        hangbridge_hor,
        hangbridge_ver,
        holy_symbol,
        horn,
        hound,
        hunting_horror,
        iron_spike,
        khaga,
        lantern,
        leech,
        leng_elder,
        lever_left,
        lever_right,
        lockpick,
        locust,
        machete,
        mantis,
        mass_of_worms,
        medical_bag,
        mi_go,
        mi_go_armor,
        mirror,
        molotov,
        monolith,
        morphic_blaster,
        mummy,
        ooze,
        orb,
        petroglyph,
        phantasm,
        pharaoh_staff,
        pillar,
        pillar_broken,
        pillar_inscribed,
        pistol,
        pit,
        pitchfork,
        player_axe,
        player_club,
        player_dagger,
        player_electric_gun,
        player_flagellant_whip,
        player_ghoul_axe,
        player_ghoul_club,
        player_ghoul_dagger,
        player_ghoul_electric_gun,
        player_ghoul_hammer,
        player_ghoul_hatchet,
        player_ghoul_machete,
        player_ghoul_morphic_blaster,
        player_ghoul_pharaoh_staff,
        player_ghoul_pistol,
        player_ghoul_pitchfork,
        player_ghoul_pump_shotgun,
        player_ghoul_rifle,
        player_ghoul_sawed_off,
        player_ghoul_sledgehammer,
        player_ghoul_spear,
        player_ghoul_spike_gun,
        player_ghoul_spiked_mace,
        player_ghoul_tommy_gun,
        player_hammer,
        player_hatchet,
        player_machete,
        player_morphic_blaster,
        player_pharaoh_staff,
        player_pistol,
        player_pitchfork,
        player_pump_shotgun,
        player_rifle,
        player_sawed_off,
        player_sledgehammer,
        player_spear,
        player_spike_gun,
        player_spiked_mace,
        player_tommy_gun,
        player_unarmed,
        polyp,
        potion,
        projectile_std_back_slash,
        projectile_std_dash,
        projectile_std_front_slash,
        projectile_std_vertical,
        pylon_angled,
        pylon_arched,
        pylon_coiled,
        pylon_serrated,
        pylon_star_crowned,
        pylon_two_pronged,
        rat,
        rat_thing,
        raven,
        revolver,
        rifle,
        ring,
        rock,
        rod,
        rubble_high,
        rubble_low,
        sarcophagus,
        scorched_ground,
        scroll,
        shadow,
        shotgun,
        sledgehammer,
        smoke,
        snake,
        spear,
        spider,
        spiked_mace,
        square_checkered,
        square_checkered_sparse,
        stairs_down,
        stalagmite,
        tentacle_cluster,
        the_high_priest,
        tomb_closed,
        tomb_open,
        tome,
        tommy_gun,
        torture_collar,
        trap_general,
        trapez,
        tree,
        tree_fungi,
        troglodyte,
        urn,
        urn_inscribed,
        vines,
        void_traveler,
        vortex,
        wall_cave_front,
        wall_cave_top,
        wall_egypt_front,
        wall_egypt_top,
        wall_front,
        wall_front_alt1,
        wall_mi_go_front,
        wall_mi_go_top,
        wall_top,
        water,
        web,
        whip_scourge,
        witch_eye,
        witch_or_warlock,
        wolf,
        worm,
        wraith,
        zombie_armed,
        zombie_bloated,
        zombie_dust,
        zombie_unarmed,
        zombie_winged,

        END
};

P character_pos(char character);

TileId str_to_tile_id(const std::string& str);

std::string tile_id_to_filename(TileId id);

}  // namespace gfx

#endif  // GFX_HPP
