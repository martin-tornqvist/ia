// =============================================================================
// Copyright Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef MAP_CONTROLLER_HPP
#define MAP_CONTROLLER_HPP

#include <memory>

// -----------------------------------------------------------------------------
// MapController
// -----------------------------------------------------------------------------
class MapController
{
public:
        MapController() = default;

        virtual ~MapController() = default;

        virtual void on_enter() {}

        virtual void on_std_turn() {}
};

class MapControllerStd : public MapController
{
public:
        MapControllerStd() = default;

        void on_enter() override;

        void on_std_turn() override;
};

class MapControllerEgypt : public MapController
{
public:
        MapControllerEgypt() = default;

        void on_enter() override;

        void on_std_turn() override;

private:
        bool m_has_triggered_awareness {false};
};

class MapControllerDeepOneLair : public MapController
{
public:
        MapControllerDeepOneLair() = default;

        void on_enter() override;
};

class MapControllerBoss : public MapController
{
public:
        MapControllerBoss() = default;

        void on_enter() override;

        void on_std_turn() override;
};

// -----------------------------------------------------------------------------
// map_control
// -----------------------------------------------------------------------------
namespace map_control
{
extern std::unique_ptr<MapController> g_controller;

}  // namespace map_control

#endif  // MAP_CONTROLLER_HPP
