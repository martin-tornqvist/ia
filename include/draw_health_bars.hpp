// =============================================================================
// Copyright Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef DRAW_HEALTH_BARS_HPP
#define DRAW_HEALTH_BARS_HPP

void draw_health_bars();

#endif  // DRAW_HEALTH_BARS_HPP
