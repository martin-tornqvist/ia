// =============================================================================
// Copyright Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef ACTOR_ACT_HPP
#define ACTOR_ACT_HPP

namespace actor
{
class Actor;

void act(Actor& actor);

}  // namespace actor

#endif  // ACTOR_ACT_HPP
