// =============================================================================
// Copyright Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef MAP_MODE_GUI_HPP
#define MAP_MODE_GUI_HPP

namespace map_mode_gui
{
void draw();

}  // namespace map_mode_gui

#endif  // MAP_MODE_GUI_HPP
