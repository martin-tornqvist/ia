// =============================================================================
// Copyright Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef AUTO_INTERACT_HPP
#define AUTO_INTERACT_HPP

namespace auto_interact
{
void run();
}  // namespace auto_interact

#endif  // AUTO_INTERACT_HPP
