// =============================================================================
// Copyright Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef ITEM_POTION_HPP
#define ITEM_POTION_HPP

#include <string>
#include <vector>

#include "colors.hpp"
#include "global.hpp"
#include "item.hpp"

namespace actor
{
class Actor;
}  // namespace actor
namespace item
{
struct ItemData;
}  // namespace item
struct P;

namespace potion
{
enum class PotionAlignment
{
        good,
        bad
};

void init();

void save();
void load();

class Potion : public item::Item
{
public:
        Potion(item::ItemData* item_data);

        virtual ~Potion() = default;

        Color interface_color() const final;

        std::string name_info_str(ItemNameIdentified id_type) const final;

        ConsumeItem activate(actor::Actor* actor) final;

        std::vector<std::string> descr_hook() const final;

        void on_collide(const P& pos, actor::Actor* actor);

        void identify(Verbose verbose) final;

        void reveal_alignment() const;

        virtual std::string real_name() const = 0;

        virtual PotionAlignment alignment() const = 0;

protected:
        virtual std::string descr_identified() const = 0;

        virtual void collide_hook(const P& pos, actor::Actor* actor) = 0;

        virtual void quaff_impl(actor::Actor& actor) = 0;

private:
        std::string alignment_str() const;
};

class Vitality : public Potion
{
public:
        Vitality(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Vitality() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Spirit : public Potion
{
public:
        Spirit(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Spirit() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Blindness : public Potion
{
public:
        Blindness(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Blindness() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Paral : public Potion
{
public:
        Paral(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Paral() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Disease : public Potion
{
public:
        Disease(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Disease() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Conf : public Potion
{
public:
        Conf(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Conf() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Fortitude : public Potion
{
public:
        Fortitude(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Fortitude() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Poison : public Potion
{
public:
        Poison(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Poison() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Insight : public Potion
{
public:
        Insight(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Insight() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Curing : public Potion
{
public:
        Curing(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Curing() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Resistance : public Potion
{
public:
        Resistance(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Resistance() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Descent : public Potion
{
public:
        Descent(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Descent() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Skill : public Potion
{
public:
        Skill(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Skill() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Carapace : public Potion
{
public:
        Carapace(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Carapace() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Blinking : public Potion
{
public:
        Blinking(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Blinking() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

class Burrowing : public Potion
{
public:
        Burrowing(item::ItemData* const item_data) :
                Potion(item_data) {}
        ~Burrowing() = default;

        void quaff_impl(actor::Actor& actor) override;
        std::string real_name() const override;
        std::string descr_identified() const override;
        PotionAlignment alignment() const override;
        void collide_hook(const P& pos, actor::Actor* actor) override;
};

}  // namespace potion

#endif  // ITEM_POTION_HPP
