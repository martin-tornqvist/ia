// =============================================================================
// Copyright Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef STUDY_INSCRIPTION_HPP
#define STUDY_INSCRIPTION_HPP

namespace study_inscription
{
void run();
}  // namespace study_inscription

#endif  // STUDY_INSCRIPTION_HPP
