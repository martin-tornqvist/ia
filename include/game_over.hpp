// =============================================================================
// Copyright Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef GAME_OVER_HPP
#define GAME_OVER_HPP

void on_game_over();

#endif  // GAME_OVER_HPP
