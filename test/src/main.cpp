// =============================================================================
// Copyright Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#define CATCH_CONFIG_MAIN  // Configures Catch2 to provide a main function
#include "catch.hpp"
