// =============================================================================
// Copyright Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef TEST_UTILS_HPP
#define TEST_UTILS_HPP

namespace test_utils
{
// Initialize a full game session
void init_all();

void cleanup_all();

}  // namespace test_utils

#endif  // TEST_UTILS_HPP
